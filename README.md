# PC Setup


## Ubuntu 24.04

```bash
sudo apt -y install curl
curl https://gitlab.com/rbertoncelj/pc-setup/-/raw/main/ubuntu/24.04/setup.sh?inline=false -o setup.sh
chmod ug+x setup.sh
./setup.sh

# Install JetBrains IDEs
~/development/tools/jetbrains-toolbox &

# Run for the first time to login
onedrive

# Enable and start the service
systemctl --user --now enable onedrive
```


## Fedora 41

```bash
sudo dnf -y install curl
curl https://gitlab.com/rbertoncelj/pc-setup/-/raw/main/fedora/41/setup.sh?inline=false -o setup.sh
chmod ug+x setup.sh
./setup.sh

# Install JetBrains IDEs
~/development/tools/jetbrains-toolbox &

# Run for the first time to login
onedrive

# Enable and start the service
systemctl --user --now enable onedrive
```


## Pop!_OS 22.04

```bash
sudo apt -y install curl
curl https://gitlab.com/rbertoncelj/pc-setup/-/raw/main/popos/22.04/setup.sh?inline=false -o setup.sh
chmod ug+x setup.sh
./setup.sh

# Install JetBrains IDEs
~/development/tools/jetbrains-toolbox &

# Run for the first time to login
onedrive

# Enable and start the service
systemctl --user --now enable onedrive
```

## TUXEDO OS 4

```bash
sudo apt -y install curl
curl https://gitlab.com/rbertoncelj/pc-setup/-/raw/main/tuxedo/4/setup.sh?inline=false -o setup.sh
chmod ug+x setup.sh
./setup.sh

# Install JetBrains IDEs
~/development/tools/jetbrains-toolbox &

# Run for the first time to login
onedrive

# Enable and start the service
systemctl --user --now enable onedrive
```