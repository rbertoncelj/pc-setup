#!/bin/bash

set -e

nvm_version="0.39.7"
toolbox_version="2.1.3.18901"
maven_version="3.9.6"
kse_version="5.5.3"
pia_version="3.5.3-07926"

if [ "$EUID" -eq 0 ]
  then echo "This script should NOT be run as root!"
  exit
fi

# Create directories for development tools and projects
mkdir -p ~/development/tools ~/development/projects

# Update & install lots of stuff...
sudo apt update -y
sudo apt upgrade -y
sudo apt dist-upgrade -y

sudo apt -y install terminator git htop iftop iotop mc vim wget curl unzip curl \
  openjdk-17-jdk openjdk-11-jdk openjdk-8-jdk visualvm python-is-python3 python3 python3-pip \
  keepassxc virtualbox \
  tmux apt-transport-https gimp inkscape onedrive peek flameshot \
  powertop speedtest-cli ca-certificates gnupg lsb-release libfuse2

# Configure system to use JDK 11 by default
sudo update-java-alternatives --set java-1.11.0-openjdk-amd64
echo 'export JAVA_HOME=$(readlink -f /usr/bin/javac | sed "s:/bin/javac::")' >> ~/.profile

# Install maven
wget https://dlcdn.apache.org/maven/maven-3/$maven_version/binaries/apache-maven-$maven_version-bin.tar.gz
sudo tar -xvf apache-maven-$maven_version-bin.tar.gz -C /opt
rm -f apache-maven-$maven_version-bin.tar.gz
sudo ln -s /opt/apache-maven-$maven_version /opt/maven
echo 'export M2_HOME=/opt/maven' >> ~/.profile
echo 'export MAVEN_HOME=/opt/maven' >> ~/.profile
echo 'export PATH=${M2_HOME}/bin:${PATH}' >> ~/.profile

# Install Docker
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin
sudo usermod -aG docker $USER
sudo tee /usr/bin/docker-compose > /dev/null <<EOT
#!/bin/bash
docker compose \$@
EOT
sudo chmod ugo+x /usr/bin/docker-compose

# Install NVM and latest LTS version of Node.js
curl -o- "https://raw.githubusercontent.com/nvm-sh/nvm/v${nvm_version}/install.sh" | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
nvm install --lts

# Vagrant
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install vagrant

# Keystore Explorer
wget -O /tmp/kse_${kse_version}_all.deb https://github.com/kaikramer/keystore-explorer/releases/download/v${kse_version}/kse_${kse_version}_all.deb
sudo apt install -y /tmp/kse_${kse_version}_all.deb
rm -f /tmp/kse_${kse_version}_all.deb

# Install Brave browser
sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt update -y
sudo apt install -y brave-browser


# Install Visual Studio Code
wget -O /tmp/visual-studio-code.deb https://go.microsoft.com/fwlink/?LinkID=760868
sudo apt -y install /tmp/visual-studio-code.deb
rm -f /tmp/visual-studio-code.deb

# JetBrains toolbox
wget -O /tmp/jetbrains-toolbox.tar.gz "https://download-cdn.jetbrains.com/toolbox/jetbrains-toolbox-${toolbox_version}.tar.gz"
tar -xvf /tmp/jetbrains-toolbox.tar.gz -C /tmp/
mv "/tmp/jetbrains-toolbox-${toolbox_version}/jetbrains-toolbox" ~/development/tools/

# Ansible
python -m pip install --user ansible

# Minikube
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
rm -f minikube_latest_amd64.deb

# Apps from Flatpak
flatpak install -y \
  com.bitwarden.desktop \
  com.spotify.Client \
  com.slack.Slack \
  com.skype.Client \
  com.sindresorhus.Caprine \
  us.zoom.Zoom \
  io.github.Figma_Linux.figma_linux

# Viber
wget https://download.cdn.viber.com/cdn/desktop/Linux/viber.deb
sudo apt install -y ./viber.deb
rm -f viber.deb

# Webex Teams
wget https://binaries.webex.com/WebexDesktop-Ubuntu-Official-Package/Webex.deb
sudo apt install -y ./Webex.deb
rm -f Webex.deb

# Signal
wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
cat signal-desktop-keyring.gpg | sudo tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' |\
  sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
sudo apt update -y && sudo apt install -y signal-desktop
rm -f signal-desktop-keyring.gpg

# PIA VPN
wget https://installers.privateinternetaccess.com/download/pia-linux-${pia_version}.run
chmod ug+x pia-linux-${pia_version}.run
./pia-linux-${pia_version}.run
rm -f pia-linux-${pia_version}.run

# Cryptomator
sudo add-apt-repository -y ppa:sebastian-stenzel/cryptomator
sudo apt update -y
sudo apt install -y cryptomator

# cleanup
sudo apt -y autoremove
sudo apt -y autoclean
