#!/bin/bash

set -e
set -x

nvm_version="0.40.0"
maven_version="3.9.9"
toolbox_version="2.5.1.34629"
kse_version="5.5.3"
pia_version="3.6.1-08339"

if [ "$EUID" -eq 0 ]
  then echo "This script should NOT be run as root!"
  exit
fi

# Create directories for development tools and projects
mkdir -p ~/development/tools ~/development/projects

# Update & install lots of stuff...
sudo apt update -y
sudo apt full-upgrade -y

sudo apt -y install \
  apt-transport-https \
  ca-certificates \
  curl \
  curl \
  flameshot \
  gimp \
  git \
  gnupg \
  htop \
  iftop \
  inkscape \
  iotop \
  keepassxc \
  libcharon-extauth-plugins \
  libcharon-extra-plugins \
  libfuse2 \
  lsb-release \
  mc \
  onedrive \
  openjdk-11-jdk \
  openjdk-17-jdk \
  openjdk-21-jdk \
  openjdk-8-jdk \
  peek \
  pipx \
  powertop \
  python-is-python3 \
  python3 \
  solaar \
  speedtest-cli \
  strongswan-nm \
  terminator \
  tmux \
  unzip \
  vim \
  virtualbox \
  visualvm \
  wget

# Java & Maven
curl -s "https://get.sdkman.io" | bash
source "/home/rok/.sdkman/bin/sdkman-init.sh"
sdk install java 21-local /usr/lib/jvm/java-21-openjdk-amd64
sdk install java 17-local /usr/lib/jvm/java-17-openjdk-amd64
sdk install java 11-local /usr/lib/jvm/java-11-openjdk-amd64
sdk install java 8-local  /usr/lib/jvm/java-8-openjdk-amd64
sdk default java 17-local
sdk install maven 3.9.9
sdk install quarkus
sdk install quarkus 2.16.12.Final
sdk default quarkus 2.16.12.Final
sdk install micronaut
sdk install micronaut 3.8.12
sdk default micronaut 3.8.12

# Install Docker
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin
sudo usermod -aG docker $USER
sudo tee /usr/bin/docker-compose > /dev/null <<EOT
#!/bin/bash
docker compose  \$@
EOT
sudo chmod ugo+x /usr/bin/docker-compose

# Install NVM and latest LTS version of Node.js
curl -o- "https://raw.githubusercontent.com/nvm-sh/nvm/v${nvm_version}/install.sh" | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] &&  \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] &&  \. "$NVM_DIR/bash_completion"
nvm install --lts

# Vagrant
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install vagrant

# Keystore Explorer
wget -O /tmp/kse_${kse_version}_all.deb https://github.com/kaikramer/keystore-explorer/releases/download/v${kse_version}/kse_${kse_version}_all.deb
sudo apt install -y /tmp/kse_${kse_version}_all.deb
rm -f /tmp/kse_${kse_version}_all.deb

# Install Brave browser
sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt update -y
sudo apt install -y brave-browser

# Install Visual Studio Code
wget -O /tmp/visual-studio-code.deb https://go.microsoft.com/fwlink/?LinkID=760868
sudo apt -y install /tmp/visual-studio-code.deb
rm -f /tmp/visual-studio-code.deb

# JetBrains toolbox
wget -O /tmp/jetbrains-toolbox.tar.gz "https://download-cdn.jetbrains.com/toolbox/jetbrains-toolbox-${toolbox_version}.tar.gz"
tar -xvf /tmp/jetbrains-toolbox.tar.gz -C /tmp/
mv "/tmp/jetbrains-toolbox-${toolbox_version}/jetbrains-toolbox" ~/development/tools/

# Ansible
python -m pipx install --include-deps ansible

# Minikube
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
rm -f minikube_latest_amd64.deb

# Apps from Flatpak
flatpak install -y  \
  com.bitwarden.desktop  \
  com.spotify.Client  \
  com.slack.Slack  \
  com.skype.Client  \
  us.zoom.Zoom  \
  com.sindresorhus.Caprine  \
  org.signal.Signal  \
  org.cryptomator.Cryptomator  \
  io.github.Figma_Linux.figma_linux  \
  com.stremio.Stremio

# Viber
wget https://download.cdn.viber.com/cdn/desktop/Linux/viber.deb
sudo apt install -y ./viber.deb
rm -f viber.deb

# PIA VPN
wget https://installers.privateinternetaccess.com/download/pia-linux-${pia_version}.run
chmod ug+x pia-linux-${pia_version}.run
./pia-linux-${pia_version}.run
rm -f pia-linux-${pia_version}.run

# cleanup
sudo apt -y autoremove
sudo apt -y autoclean
