#!/bin/bash

set -e

nvm_version="0.40.0"
visualvm_version="2.1.9"
toolbox_version="2.4.2.32922"
kse_version="5.5.3"
pia_version="3.5.7-08120"

if [ "$EUID" -eq 0 ]
  then echo "This script should NOT be run as root!"
  exit
fi

set -x


# Create directories for development tools and projects
mkdir -p ~/development/tools ~/development/projects

# Configure DNF
echo "max_parallel_downloads=20" | sudo tee -a /etc/dnf/dnf.conf

# Additional software repos
sudo dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf copr enable -y kwizart/fedy
sudo dnf config-manager -y --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/
sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc
sudo dnf config-manager -y --add-repo https://packages.microsoft.com/yumrepos/vscode
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo dnf config-manager -y --add-repo https://download.docker.com/linux/fedora/docker-ce.repo

# Install lots of stuff...
sudo dnf install -y \
  fedy \
  terminator \
  git \
  htop \
  iftop \
  iotop \
  mc \
  vim \
  wget \
  curl \
  unzip \
  pigz \
  pv \
  jq \
  java-1.8.0-openjdk-devel \
  java-11-openjdk-devel \
  java-17-openjdk-devel \
  java-21-openjdk-devel \
  python3 \
  python3-pip \
  NetworkManager-openvpn-gnome \
  keepassxc \
  gnome-tweaks \
  gnome-extensions-app \
  VirtualBox \
  tmux \
  vagrant \
  dnf-plugins-core \
  brave-browser \
  code \
  ansible \
  gnome-shell-extension-pop-shell \
  gimp \
  inkscape \
  docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Java & Maven
curl -s "https://get.sdkman.io" | bash
source "/home/rok/.sdkman/bin/sdkman-init.sh"
sdk install java 21-local /usr/lib/jvm/java-21
sdk install java 17-local /usr/lib/jvm/java-17
sdk install java 11-local /usr/lib/jvm/java-11
sdk install java 8-local /usr/lib/jvm/java-1.8.0
sdk default java 21-local
sdk install maven 3.9.9
sdk install quarkus
sdk install quarkus 2.16.12.Final
sdk default quarkus 2.16.12.Final
sdk install micronaut
sdk install micronaut 3.8.12
sdk default micronaut 3.8.12

# VisualVM
visualvm_short_version=$(echo ${visualvm_version} | sed 's/\.//g')
wget -P /tmp https://github.com/oracle/visualvm/releases/download/${visualvm_version}/visualvm_${visualvm_short_version}.zip
sudo unzip /tmp/visualvm_${visualvm_short_version}.zip -d /opt/
rm -f /tmp/visualvm_${visualvm_short_version}.zip
sudo mv /opt/visualvm_${visualvm_short_version}/ /opt/visualvm
cat > /tmp/visualvm.desktop << EOF
[Desktop Entry]
Encoding=UTF-8
Name=VisualVM
Exec=/opt/visualvm/bin/visualvm
Icon=/opt/visualvm/etc/visualvm.icns
Type=Application
Categories=Development;
EOF
sudo mv /tmp/visualvm.desktop /usr/share/applications/visualvm.desktop
sudo chown root:root /usr/share/applications/visualvm.desktop
sudo chmod 0644 /usr/share/applications/visualvm.desktop

# JetBrains Toolbox
wget -O /tmp/jetbrains-toolbox.tar.gz "https://download-cdn.jetbrains.com/toolbox/jetbrains-toolbox-${toolbox_version}.tar.gz"
tar -xvf /tmp/jetbrains-toolbox.tar.gz -C /tmp/
mv "/tmp/jetbrains-toolbox-${toolbox_version}/jetbrains-toolbox" ~/development/tools/

 
# Complete docker setup
sudo systemctl enable --now docker.service
sudo systemctl enable --now containerd.service
sudo usermod -aG docker $USER
 
# Install NVM and latest LTS version of Node.js
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v${nvm_version}/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
nvm install --lts
 
# Keystore Explorer
wget -O /tmp/kse.rpm https://github.com/kaikramer/keystore-explorer/releases/download/v${kse_version}/kse-${kse_version}-1.noarch.rpm
sudo dnf install -y /tmp/kse.rpm
rm -f /tmp/kse.rpm

# Minikube
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-latest.x86_64.rpm
sudo rpm -Uvh minikube-latest.x86_64.rpm
rm -f minikube-latest.x86_64.rpm

# Install stuff from flathub
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install -y \
  com.bitwarden.desktop \
  com.spotify.Client \
  com.slack.Slack \
  com.skype.Client \
  com.sindresorhus.Caprine \
  org.signal.Signal \
  org.cryptomator.Cryptomator \
  io.github.Figma_Linux.figma_linux

# Zoom
wget https://zoom.us/client/latest/zoom_x86_64.rpm
sudo dnf install -y zoom_x86_64.rpm
rm -f zoom_x86_64.rpm

# Viber
wget https://download.cdn.viber.com/desktop/Linux/viber.rpm
sudo dnf install -y viber.rpm
rm -f viber.rpm

# PIA VPN
wget https://installers.privateinternetaccess.com/download/pia-linux-${pia_version}.run
chmod ug+x pia-linux-${pia_version}.run
./pia-linux-${pia_version}.run
rm -f pia-linux-${pia_version}.run

# TUXEDO Computers drivers
manufacturer=$(sudo dmidecode -s system-manufacturer)
if [ "$manufacturer" = "TUXEDO" ]; then
  sudo dnf -y install kernel-devel
  sudo dnf -y copr enable kallepm/tuxedo-drivers
  sudo dnf -y install tuxedo-drivers
  sudo dnf -y copr enable kallepm/tuxedo-control-center
  sudo dnf -y install tuxedo-control-center
fi
