#!/bin/bash

set -e
set -x

nvm_version="0.40.0"
maven_version="3.9.9"
toolbox_version="2.4.2.32922"
kse_version="5.5.3"
pia_version="3.6.1-08339"
caprine_version="2.60.1"
tomte_version="2.39.2"

 
if [ "$EUID" -eq 0 ]
  then echo "This script should NOT be run as root!"
  exit
fi

 
# Create directories for development tools and projects
mkdir -p ~/development/tools ~/development/projects
 
# Update & install lots of stuff...
sudo apt update -y
sudo apt upgrade -y
sudo apt dist-upgrade -y 
sudo snap refresh

sudo apt -y install \
  terminator git htop iftop iotop mc vim wget curl unzip curl \
  openjdk-21-jdk openjdk-17-jdk openjdk-11-jdk openjdk-8-jdk visualvm solaar \
  python-is-python3 python3 pipx \
  network-manager-openvpn-gnome keepassxc gnome-tweaks virtualbox touchegg \
  tmux apt-transport-https gimp inkscape onedrive peek flameshot \
  gnome-shell-extensions powertop speedtest-cli ca-certificates gnupg lsb-release libfuse2 \
  strongswan-nm libcharon-extauth-plugins libcharon-extra-plugins
 
# Configure system to use JDK 17 by default
sudo update-java-alternatives --set java-1.17.0-openjdk-amd64
echo 'export JAVA_HOME=$(readlink -f /usr/bin/javac | sed "s:/bin/javac::")' >> ~/.profile

# Install maven
wget https://dlcdn.apache.org/maven/maven-3/$maven_version/binaries/apache-maven-$maven_version-bin.tar.gz
sudo tar -xvf apache-maven-$maven_version-bin.tar.gz -C /opt
rm -f apache-maven-$maven_version-bin.tar.gz
sudo ln -s /opt/apache-maven-$maven_version /opt/maven
echo 'export M2_HOME=/opt/maven' >> ~/.profile
echo 'export MAVEN_HOME=/opt/maven' >> ~/.profile
echo 'export PATH=${M2_HOME}/bin:${PATH}' >> ~/.profile
 
# Install Docker
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin
sudo usermod -aG docker $USER
sudo tee /usr/bin/docker-compose > /dev/null <<EOT
#!/bin/bash
docker compose \$@
EOT
sudo chmod ugo+x /usr/bin/docker-compose
 
# Install NVM and latest LTS version of Node.js
curl -o- "https://raw.githubusercontent.com/nvm-sh/nvm/v${nvm_version}/install.sh" | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
nvm install --lts
 
# Vagrant
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install vagrant
 
# Keystore Explorer
wget -O /tmp/kse_${kse_version}_all.deb https://github.com/kaikramer/keystore-explorer/releases/download/v${kse_version}/kse_${kse_version}_all.deb
sudo apt install -y /tmp/kse_${kse_version}_all.deb
rm -f /tmp/kse_${kse_version}_all.deb
 
# Install Brave browser
sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt update -y
sudo apt install -y brave-browser
 
# Install Visual Studio Code
wget -O /tmp/visual-studio-code.deb https://go.microsoft.com/fwlink/?LinkID=760868
sudo apt -y install /tmp/visual-studio-code.deb
rm -f /tmp/visual-studio-code.deb

# JetBrains toolbox
wget -O /tmp/jetbrains-toolbox.tar.gz "https://download-cdn.jetbrains.com/toolbox/jetbrains-toolbox-${toolbox_version}.tar.gz"
tar -xvf /tmp/jetbrains-toolbox.tar.gz -C /tmp/
mv "/tmp/jetbrains-toolbox-${toolbox_version}/jetbrains-toolbox" ~/development/tools/
 
# Ansible
python -m pipx install ansible

# Minikube
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
rm -f minikube_latest_amd64.deb

# Apps from Snap
sudo snap install bitwarden slack skype zoom-client spotify figma-linux pomatez

# Caprine
wget https://github.com/sindresorhus/caprine/releases/download/v${caprine_version}/caprine_${caprine_version}_amd64.deb
sudo apt install -y ./caprine_${caprine_version}_amd64.deb
rm -f caprine_${caprine_version}_amd64.deb

# Viber
wget https://download.cdn.viber.com/cdn/desktop/Linux/viber.deb
sudo apt install -y ./viber.deb
rm -f viber.deb

# Signal
wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
cat signal-desktop-keyring.gpg | sudo tee -a /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' |\
  sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
sudo apt update -y && sudo apt install -y signal-desktop
rm -f signal-desktop-keyring.gpg

# Replace Firefox snap with native
sudo snap remove firefox
sudo apt remove -y firefox
sudo add-apt-repository -y ppa:mozillateam/ppa

echo '
Package: *
Pin: release o=LP-PPA-mozillateam
Pin-Priority: 1001
' | sudo tee /etc/apt/preferences.d/mozilla-firefox

echo 'Unattended-Upgrade::Allowed-Origins:: "LP-PPA-mozillateam:${distro_codename}";' | sudo tee /etc/apt/apt.conf.d/51unattended-upgrades-firefox

sudo apt install -y firefox

# PIA VPN
wget https://installers.privateinternetaccess.com/download/pia-linux-${pia_version}.run
chmod ug+x pia-linux-${pia_version}.run
./pia-linux-${pia_version}.run
rm -f pia-linux-${pia_version}.run

# Cryptomator
sudo add-apt-repository -y ppa:sebastian-stenzel/cryptomator
sudo apt update -y
sudo apt install -y cryptomator

# Touch gestures for X11
sudo add-apt-repository -y ppa:touchegg/stable
sudo apt update
sudo apt install -y touchegg

# TUXEDO Computers drivers
manufacturer=$(sudo dmidecode -s system-manufacturer)
if [ "$manufacturer" = "TUXEDO" ]; then
  wget https://deb.tuxedocomputers.com/ubuntu/pool/main/t/tuxedo-tomte/tuxedo-tomte_${tomte_version}_all.deb
  sudo apt install -y ./tuxedo-tomte_${tomte_version}_all.deb
  rm -f tuxedo-tomte_${tomte_version}_all.deb
fi

# cleanup
sudo apt -y autoremove
sudo apt -y autoclean